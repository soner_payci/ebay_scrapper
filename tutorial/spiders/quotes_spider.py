import scrapy
import re
import lxml
import time
import urllib
from lxml.html.clean import Cleaner
import unicodedata
from bs4 import BeautifulSoup
import requests

class QuotesSpider(scrapy.Spider):
    name = "quotes"
    eventList = []
    description = []
    imgUrls = []
    sayac = 0
    def start_requests(self):
        urls = [
            'https://www.ebay.com/itm/4-Miami-Dolphins-vs-New-York-Jets-Sec-106-Row-8-Aisle-Tickets-Parking-11-4-18/283150739217?hash=item41ed196711:g:6QIAAOSwvKha3zya',
            'https://www.ebay.com/itm/3-Tickets-Michigan-Wolverines-vs-Penn-State-Nittany-Lions-Football-11-3-18/273462302489?hash=item3fab9f9719:g:h0oAAOSwZRlbnDM3',
            'https://www.ebay.com/itm/Two-Michigan-State-Spartan-vs-Michigan-Wolverine-Football-Tickets-10-20-2018/183419750723?hash=item2ab4ab4943:g:e2MAAOSwWeJbgrRQ',
            'https://www.ebay.com/itm/2-Tickets-Boston-Celtics-Philadelphia-76ers-October-16-2018-Opening-Night/192628254988?hash=item2cd989e90c:g:EdUAAOSw3uRbdGTe',
            'https://www.ebay.com/itm/ELTON-JOHN-CONCERT-TICKETS-2-VAN-ANDEL-ARENA-GRAND-RAPIDS-MI-OCT-15/152908078240?hash=item239a081ca0:g:diUAAOSwnsRafvts',
            'https://www.ebay.com/itm/LSU-VS-ALABAMA-4-TICKETS-11-3-2018-TIGER-STADIUM-GREAT-SEATS-FREE-SHIPPING/283147948387?hash=item41eceed163:g:B5oAAOSwU1hbkyUp',
            'https://www.ebay.com/itm/4-DENVER-BRONCOS-vs-BALTIMORE-RAVENS-TICKETS-9-23-2018-at-M-T-Bank-LOWERS-L-K/253824810002?hash=item3b19233c12:g:I3kAAOSwY8Bbe5OO',
            'https://www.ebay.com/itm/2-Tickets-Detroit-Lions-vs-New-England-Patriots-9-23-18-50-YARD-LINE-ROW-5/302846386950?hash=item46830d2306:g:or0AAOSwp7RbdNjC',
            'https://www.ebay.com/itm/2-New-England-Patriots-VS-Buffalo-Bills-Tickets-Lower-Level-Section-105/401588152426?hash=item5d8084e86a:g:2VoAAOSw-6pbX0dt',
            'https://www.ebay.com/itm/Washington-Redskins-vs-Packers-9-23-18-2-LL-1-platinum-parking/173519060980?hash=item28668abff4:g:SwQAAOSwI5VbaM93',
            'https://www.ebay.com/itm/Patriots-vs-Colts-2-seats-Sec-236-Row-18/132774513861?hash=item1ee9fa4cc5:g:fH4AAOSwuMNbX2xA',
            'https://www.ebay.com/itm/2-Tickets-New-York-Yankees-at-Boston-Red-Sox-9-29-18-Fenway-Park/273337763407?hash=item3fa433464f:g:InEAAOSwUPZbPLyn',
            'https://www.ebay.com/itm/4-Tickets-Brian-Setzer-Orchestra-11-18-18-Fox-Theatre-Detroit-Detroit-MI/273429787664?hash=item3fa9af7410:g:guMAAOSwvoNbhIAF',
            'https://www.ebay.com/itm/3-Chicago-Bears-vs-Tampa-Bay-40-Yard-Line-Football-Tickets-Sun-Sept-30-2018/163248515043?hash=item26025ea7e3:g:gtsAAOSwnN5birCT',
            'https://www.ebay.com/itm/2-Red-Sox-Tickets-vs-New-York-Yankees-9-29-18-SEC-14-ROW-8-MINT-SEATS-RIVALRY/223140206267?hash=item33f43156bb:g:yTEAAOSwMPxaxuyT',
            'https://www.ebay.com/itm/2-Tickets-Chicago-Blackhawks-Minnesota-Wild-10-11-18-Saint-Paul-MN/273458349946?hash=item3fab63477a:g:bgAAAOSwj4dbmXXI'
                ]

        for url in urls:
            #yield scrapy.Request(url=url, callback=self.parse)
            yield scrapy.Request(url=url, callback=self.parse, priority = 10)

    

            
    def parse_iframe(self, response):
        html = response.xpath("//body/*").extract()[0]
        cleaner = Cleaner()
        cleaner.javascript = True
        cleaner.style = True
        html = unicodedata.normalize("NFKD", html).encode('ascii','ignore')
        clean = cleaner.clean_html(html) 
        soup = BeautifulSoup(clean, 'lxml')
        description = soup.get_text()
        self.description.append(description)
        self.sayac +=1
        for i in range (0,len(self.description)): 
            with open('events.txt', 'a') as file:
                file.write(
                        ('\n'*2) +
                        str(self.sayac) + '. EVENT\n\n\n' +
                        self.eventList[i]['title'] + '\n\t' +
                        self.eventList[i]['price'] +
                        str(self.description[i]) +
                        self.eventList[i]['details'] + 
                        ('\n'*5) + ('='*30)
                )
            for i in range (0,len(self.imgUrls)):
                with open(str(self.sayac) + '.jpg', 'wb') as f:
                    f.write(requests.get(self.imgUrls[i]).content)
            self.eventList.pop(0)

        '''img_data = requests.get(self.eventList[i]['imgUrl']).content
        with open(i+'.jpg', 'wb') as imageFile:
            imageFile.write(img_data)'''
        self.description.pop()

    def parse(self, response):
        descriptionUrl = response.css('iframe::attr(src)').extract_first() # descriptionUrl ready

        event = {
            'title': '',
            'details': '',
            'imgUrl': ''
        }
        html = response.xpath("//body/*").extract()[0]
        cleaner = Cleaner()
        cleaner.javascript = True
        cleaner.style = True
        html = unicodedata.normalize("NFKD", html).encode('ascii','ignore')
        clean = cleaner.clean_html(html)
        imgUrls = response.css('img').xpath('@src').extract()#[3].replace('l300', 'l1600') # imgUrls ready
        for img in imgUrls:
            if ('l300' in img):
                imgUrl = img
                break
        imgUrl.replace('l300', 'l1600')
        self.imgUrls.append(imgUrl)
        title = response.selector.xpath('//title/text()\n').extract() # title ready
        price = response.selector.css('span#prcIsum::text').extract()[0]
        for td in response.selector.xpath('//div[@class="itemAttr"]//div//table').extract():
            html = td
            cleaner = Cleaner()
            cleaner.javascript = True
            cleaner.style = True
            html = unicodedata.normalize("NFKD", html).encode('ascii','ignore')
            clean = cleaner.clean_html(html)
            soup = BeautifulSoup(clean, 'lxml')
            details = soup.get_text()
            details = details.replace('\t', '@@')
            details = details.replace('\n', '\t')
            details = details.replace('@@@@@@@@@@@@@@@@@@ @@@@@@', '\n')
            details = details.replace('\t\t', ' ')
            event['title'] = title[0].split('|')[0]
            event['price'] = price
            event['details'] = details
            event['imgUrl'] = imgUrl
        self.eventList.append(event)   
        yield scrapy.Request(url=descriptionUrl, callback=self.parse_iframe, priority = 100)
        '''for i in range (0,len(self.eventList)): 
            with open('events.txt', 'w') as file:
                file.write('\n\n\n' +
                        self.eventList[i]['title'] +
                        #str(self.description[i]) +
                        self.eventList[i]['details'] +('\n'*5) + ('='*100)
                )'''




